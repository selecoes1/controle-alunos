$(function() {

    $("#filtrarEscolas").submit(function(e) {

        e.preventDefault();
        var form = this;
        var escola = $("#escola").val();
        var bairro = $("#bairro").val();
        var cidade = $("#cidade").val();
        var uf = $("#uf").val();

        escola = escola.replace(/<.*?>/g, '').trim();
        bairro = bairro.replace(/<.*?>/g, '').trim();
        cidade = cidade.replace(/<.*?>/g, '').trim();
        uf = uf.replace(/<.*?>/g, '').trim();

        $("#escola").val(escola);
        $("#bairro").val(bairro);
        $("#cidade").val(cidade);
        $("#uf").val(uf);

        form.submit();
    });
});