 $(function() {

        $("#filtrarAlunos").submit(function(e) {

            e.preventDefault();
            var form = this;
            var nome = $("#nome").val();
            var telefone = $("#telefone").val();
            var email = $("#email").val();
            var dtnascimento = $("#dtnascimento").val();
            var genero = $("#genero").val();

            nome = nome.replace(/<.*?>/g, '').trim();
            telefone = telefone.replace(/<.*?>/g, '').trim();
            email = email.replace(/<.*?>/g, '').trim();
            dtnascimento = dtnascimento.replace(/<.*?>/g, '').trim();
            genero = genero.replace(/<.*?>/g, '').trim();

            $("#nome").val(nome);
            $("#telefone").val(telefone);
            $("#email").val(email);
            $("#dtnascimento").val(dtnascimento);
            $("#genero").val(genero);

            form.submit();
        });
    });