<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome'); // conforme doumentação é campo obrigatório
            $table->string('telefone')->nullable();
            $table->string('email')->unique(); // conforme doumentação é campo obrigatório / add como unico tb
            $table->date('dtnascimento')->nullable();
            $table->string('genero')->nullable(); //acredito ser masculino, feminino
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos');
    }
}
