<?php

use Faker\Generator as Faker;
use App\ModelAlunoTurma;
use App\ModelAluno;

$factory->define(App\ModelAlunoTurma::class, function (Faker $faker) {



    $aluno = $faker->randomElement(ModelAluno::lists('id'));

    dd($aluno);

    
    
    $turma = $faker->randomElement($array = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26));

    // dd($aluno);

    return [
        'aluno_id' => $aluno,     
        'turma_id' => $turma, 
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
