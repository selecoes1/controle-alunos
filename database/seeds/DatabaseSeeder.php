<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AlunosTableSeeder::class);
        $this->call(EscolasTableSeeder::class);
        $this->call(TurmasTableSeeder::class);
        $this->call(AlunoTurmaTableSeeder::class);
    }
}
