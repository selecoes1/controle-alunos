<?php

use Illuminate\Database\Seeder;
use App\ModelEscola;

class EscolasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ModelEscola::class, 6)->create();
    }
}
