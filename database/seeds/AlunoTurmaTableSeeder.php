<?php

use Illuminate\Database\Seeder;
use App\ModelAlunoTurma;

class AlunoTurmaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ModelAlunoTurma::class, 650)->create();
    }
}
