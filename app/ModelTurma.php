<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTurma extends Model
{
    protected $table = 'turmas';

    protected $fillable = [
        'ano', 'nivel', 'serie', 'turno', 'escola_id'
    ];

    public function escola()
    {
        return $this->belongsTo(ModelEscola::class, 'escola_id', 'id');
    }

    public function alunoturma()
    {
        return $this->hasMany(ModelAlunoTurma::class, 'turma_id','id');
    }
}
