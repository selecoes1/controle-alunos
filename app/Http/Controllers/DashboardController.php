<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelAluno;
use App\ModelEscola;
use App\ModelTurma;

class DashboardController extends Controller
{
    public function dashboard()
    {

        $alunos = ModelAluno::count();
        $escolas = ModelEscola::count();
        $turmas = ModelTurma::count();

        $widget = [
            'alunos' => $alunos,
            'escolas' => $escolas,
            'turmas' => $turmas,
        ];
        return view('home', compact('widget'));
    }

    public function servicoApiAlunos(){
        $alunos = ModelAluno::all();
        return response()->json($alunos);
    }

    public function servicoApiTurmas(){
        $turmas = ModelTurma::all();
        return response()->json($turmas);
    }

    public function servicoApiEscolas(){
        $escolas = ModelEscola::all();
        return response()->json($escolas);
    }
}
