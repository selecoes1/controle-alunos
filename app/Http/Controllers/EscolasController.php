<?php

namespace App\Http\Controllers;

use App\Http\Requests\FiltrarEscolasRequest;
use App\ModelEscola;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\ModelTurma;
use App\ModelAlunoTurma;

class EscolasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('escolas.list')
        ->with('escolas', ModelEscola::orderby('escola')->paginate(5))
        ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function filtrarEscolas(FiltrarEscolasRequest $request)
    {
        $filtros = $request->except('_token');
        Log::info($filtros);

        $escolas = DB::table('escolas')
        ->select('id','escola', 'logradouro', 'bairro', 'cidade', 'uf')
        ->where(function ($query) use ($filtros) {

            if($filtros['escola'] != ''){
                $query->where('escola', 'like', '%'.$filtros['escola'].'%');
            }

            if($filtros['bairro'] != ''){
                $query->where('bairro', 'like', '%'.$filtros['bairro'].'%');
            }

            if($filtros['cidade'] != ''){
                $query->where('cidade', 'like', '%'.$filtros['cidade'].'%');
            }

            if($filtros['uf'] != ''){
                $query->where('uf', 'like', '%'.$filtros['uf'].'%');
            }
        })
        ->orderBy('escola', 'ASC')
        ->paginate(5);

        if(!count($escolas)){

            $pesquisaRetornouVazio = true;

            return view('escolas.list')
                ->with(['escolas' => $escolas, 'pesquisaRetornouVazio' => $pesquisaRetornouVazio]);
        }

        return view('escolas.list')
        ->with(['escolas' => $escolas, 'filtros' => $filtros])
        ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('escolas.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ModelEscola::create($request->all());

        $msg = "A Escola ". $request->input('escola'). " foi cadastrada com sucesso!";

        return redirect()
        ->action('EscolasController@index')
        ->with('statusSucesso', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelEscola  $modelEscola
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ModelEscola $modelEscola)
    {
        $escola = ModelEscola::find($request->input('escola_id'));
        $turma = ModelTurma::where('escola_id', $escola->id)->get();
        $turmas = ModelTurma::where('escola_id', $escola->id)->get()->count();
        // $alunos = ModelAlunoTurma::find($request->input('escola_id'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelEscola  $modelEscola
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ModelEscola $modelEscola)
    {
        $escola = ModelEscola::find($request->input('escola_id'));

        return view('escolas.form')
        ->with('escola', $escola);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelEscola  $modelEscola
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelEscola $modelEscola)
    {
        $id = $request->input('id');
        $escola = ModelEscola::find($id);

        $dados = $request->all();
        $escola->fill($dados)->save();
        $escola->save();

        $msg = "A escola ". $request->input('escola'). " foi atualizada com sucesso!";

        return redirect()
        ->action('EscolasController@index')
        ->with('statusSucesso', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelEscola  $modelEscola
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ModelEscola $modelEscola)
    {
        $id = $request->input('id');
        $turmas = ModelTurma::where('escola_id', $id)->count();

        if($turmas > 0){
            $msg = "A escola ". $request->input('escola'). " não pode ser deletada pois existem turmas vinculadas a escola!";

            return redirect()
            ->action('EscolasController@index')
            ->with('statusFalha', $msg);
        }

        $escola = ModelEscola::find($id);
        $escola->delete();


        $msg = "A escola ". $request->input('escola'). " foi deletada com sucesso!";

        return redirect()
        ->action('EscolasController@index')
        ->with('statusSucesso', $msg);
    }
}
