<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FiltrarTurmasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function prepareForValidation() {

        $this->merge([

            'ano' => trim(strip_tags($this->ano)), //strtoupper(trim(strip_tags($this->nome))), deixando a pesquina em maisculo
            'nivel' => trim(strip_tags($this->nivel)),
            'serie' => trim(strip_tags($this->serie)),
            'turno' => trim(strip_tags($this->turno)),
            'escola' => trim(strip_tags($this->escola))
        ]);
    }
}
