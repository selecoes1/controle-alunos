<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FiltrarAlunosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dtnascimento' => 'date_format:d/m/Y,|nullable'
        ];
    }

    public function messages()
    {

        return [

            'Data.date_format' => 'Formato incorreto'
        ];
    }

    public function prepareForValidation() {

        $this->merge([

            'nome' => trim(strip_tags($this->nome)), //strtoupper(trim(strip_tags($this->nome))), deixando a pesquina em maisculo
            'telefone' => trim(strip_tags($this->telefone)),
            'email' => trim(strip_tags($this->email)),
            'dtnascimento' => trim(strip_tags($this->dtnascimento)),
            'genero' => trim(strip_tags($this->genero))
        ]);
    }
}
