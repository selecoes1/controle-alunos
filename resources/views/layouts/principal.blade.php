<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{asset('/img/escola.png')}}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/principal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">

    <!--Icones-->
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <!--Google Fontes-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">


<script type="text/javascript" src="jquery.maskedinput-1.1.4.pack.js"/></script>

    <!--Inicio mascaras-->
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function($){
            $("#data").mask("99/99/9999");
            $("#tempo").mask("00:00:00");
            $("#data_tempo").mask("99/99/9999 00:00:00");
            $("#telefone").mask("(99) 9999-9999");
            $("#telefone2").mask("(99) 9999-9999");
            $("#celular").mask("(99) 99999-9999");
            $("#celular2").mask("(99) 99999-9999");
            $("#cpf").mask("999.999.999-99");
            $("#buscarcpf").mask("999.999.999-99");
            $("#cep").mask("99999-999");
            $("#cnpj").mask("99.999.999/9999-99");
            $("#placa").mask("aaa - 9999");
            $("#dinheiro").mask("000.000.000.000.000,00");
            $("#dinheiro2").mask("#.##0,00");
            $('.dinheiro').mask('#.##0,00', {reverse: true});
            $('.valor').mask('###0.00', {reverse: true}); //padrão americano
            $('.conta').mask('###########-#', {reverse:true});
            $("#dataDeCriacao").mask('99/99/9999');
            $(".numeroCalculo").mask('####/9999');
        });
    </script>
    <!--Fim marcaras-->

        <!--Inicio completa endereço com o cep-->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>


<script type="text/javascript" >

    $(document).ready(function() {
        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#endereco").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#estado").val("");
            //$("#ibge").val("");
        }

        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#estado").val("...");
                    //$("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro.toUpperCase());
                            $("#endereco").removeAttr("obrigatorio");
                            $("#bairro").val(dados.bairro.toUpperCase());
                            $("#bairro").removeAttr("obrigatorio");
                            $('#bairro-select option').filter(function() { 
                                return ($(this).text() == dados.bairro); //To select Blue
                            }).prop('selected', true);
                            $("#cidade").val(dados.localidade.toUpperCase());
                            $("#cidade").removeAttr("obrigatorio");
                            $("#estado").val(dados.uf.toUpperCase());
                            $("#estado").removeAttr("obrigatorio");
                            $("#aviso-cep").css("display","none");
                            //$("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            noCEP("CEP não encontrado.");
                            // alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    noCEP("Formato de CEP inválido.");
                    // alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                // noCEP();
                limpa_formulário_cep();
            }
        });
    });

    var inputAux;
    function noCEP(msg){
        $("#cidade").val($("#cidadeNome").val().toUpperCase());
        $("#estado").val($("#estadoNome").val().toUpperCase());
        $("#aviso-cep").css("display","inline");
        $("#aviso-cep").html(msg);
        if(!inputAux){
            inputAux =  $("#bairro-view input").get()[0];
        }
        $("#bairro-view").html($("#bairro-select").get()[0].outerHTML);
        createSelect();
    }

</script>
<!--Fim completa endereço com o cep-->

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('app.name', 'Laravel') }} {{ config('app.lastname', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
