@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="alert alert-success">
                Bem vindo, ao sistema de controle de alunos
            </div>
            <div class="card">
                <div class="card-header textoBold">Dashboard</div>

                <div class="card-body">

                    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-xl-4">
                                <a href="{{ route('alunos') }}">
                                    <div class="card bg-c-blue order-card" id='teste3'>
                                        <div class="card-block">
                                            <h6 class="m-b-20">Alunos</h6>
                                            <h2 class="text-right"><i class="fa fa-users f-left"></i><span>{{ $widget['alunos'] }}</span></h2>
                                            {{-- <p class="m-b-0">Completed Orders<span class="f-right">351</span></p> --}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                            <div class="col-md-4 col-xl-4">
                                <a href="{{ route('turmas') }}">
                                    <div class="card bg-c-green order-card" id='teste3'>
                                        <div class="card-block">
                                            <h6 class="m-b-20">Turmas</h6>
                                            <h2 class="text-right"><i class="fa fa-rocket f-left"></i><span>{{ $widget['turmas'] }}</span></h2>
                                            {{-- <p class="m-b-0">Completed Orders<span class="f-right">351</span></p> --}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                            <div class="col-md-4 col-xl-4">
                                <a href="{{ route('escolas') }}">
                                    <div class="card bg-c-yellow order-card" id='teste3'>
                                        <div class="card-block">
                                            <h6 class="m-b-20">Escolas</h6>
                                            <h2 class="text-right"><i class="fa fa-university f-left"></i><span>{{ $widget['escolas'] }}</span></h2>
                                            {{-- <p class="m-b-0">Completed Orders<span class="f-right">351</span></p> --}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                        </div>
                    </div>



                    
                </div><!--aqui-->
            </div>
        </div>
    </div>
</div>
@endsection
