@extends('layouts.principal')

@section('content')

<div class="container">
     <!--Filtro-->
     <form action="{{route('filtrarEscolas')}}" class="" id="filtrarEscolas" method="get">
        <div class="form-row">
            <div class="form-group col-md-4 formGroupMeio" id="formGroupRequerente">
                <label for="escola">Escola</label>
                <input type="text" class="form-control" id="escola" name="escola" placeholder="nome da escola"  @if(isset($filtros)) value="{{$filtros['escola']}} "@endif>
            </div>
            <div class="form-group col-md-2 formGroupMeio" id="formGroupRequerimento">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="ex: Passaré"   @if(isset($filtros)) value="{{$filtros['bairro']}}" @endif>
            </div>
            <div class="form-group col-md-3 formGroupMeio" id="formGroupServidor">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="ex:Fortaleza"   @if(isset($filtros)) value="{{$filtros['cidade']}} " @endif>
            </div>
            <div class="form-group col-md-3 formGroupMeio" id="formGroupServidor">
                <label for="uf">Estado</label>
                <input type="text" class="form-control" id="uf" name="uf" placeholder="ex: Ceará"   @if(isset($filtros)) value="{{$filtros['uf']}} " @endif>
            </div>
        </div>
        <div class="buttonRight">
            <button class="btn btn-primary">Pesquisar</button>
            <a href="{{route('escolas')}}" class="btn btn-secondary">Limpar</a>
        </div>
    </form>
    <!--Fim Filtro-->
    @if(session('statusFalha'))
        <div class="alert alert-danger">
            {{ session('statusFalha') }}
        </div>
    @elseif (session('statusSucesso'))
        <div class="alert alert-success">
            {{ session('statusSucesso') }}
        </div>
    @endif
    @if(!count($escolas) && isset($pesquisaRetornouVazio))
        <div class="alert alert-danger">
            Nenhuma escola encontrada com os parâmetros passados na pesquisa.
        </div>
    @else
    <div class="card-header textoBold">ESCOLAS</div>
        <div class="table-responsive">
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Escola</th>
                    <th scope="col">Logradouro</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Estado</th>
                    <th scope="col">
                        <div class=".col-lg-12 buttonRight">
                            <a href="{{route('new.escola')}}" class="btn btn-primary btn-sm">Add Nova Escola</a>
                        </div>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($escolas as $e)
                    <tr class="table-sem-quebra">
                        <th scope="row">{{++$i}}</th>
                        <td>{{$e->escola}}</td>
                        <td>{{$e->logradouro}}</td>
                        <td>{{$e->bairro}}</td>
                        <td>{{$e->cidade}}</td>
                        <td>{{$e->uf}}</td>
                        <td>
                            <div class=".col-lg-12 buttonRight">
                                <a href="{{route('show.escola', ['escola_id'=>$e->id])}}" class="btn btn-outline-success btn-sm">Detalhes</a>
                                <a href="{{route('edit.escola', ['escola_id'=>$e->id])}}" class="btn btn-outline-primary btn-sm">Editar</a>
                                <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#modalExcluirEscolas{{$e->id}}">Excluir</button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if(isset($filtros))
                {{$escolas->appends($filtros)->links()}}
            @else
                {{$escolas->links()}}
            @endif
        </div>
    </div>
    @endif
</div>
@foreach($escolas as $e)
@include('/modals/deleteModalEscola')
@endforeach
<script src="/js/ctrl/escolas/pesquisa.js"></script>

@endsection
