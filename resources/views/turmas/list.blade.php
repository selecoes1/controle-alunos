@extends('layouts.principal')

@section('content')

<div class="container">
     <!--Filtro-->
     <form action="{{route('filtrarTurmas')}}" class="" id="filtrarTurmas" method="get">
        <div class="form-row">
            <div class="form-group col-md-2 formGroupMeio">
                <label for="ano">Ano</label>
                <input type="text" class="form-control" id="ano" name="ano" placeholder="informe o ano"  @if(isset($filtros)) value="{{$filtros['ano']}} "@endif>
            </div>
            <div class="form-group col-md-2 formGroupMeio">
                <label for="nivel">Nível</label>
                <input type="text" class="form-control" id="nivel" name="nivel" placeholder="Médio ou Fundamental"   @if(isset($filtros)) value="{{$filtros['nivel']}}" @endif>
            </div>
            <div class="form-group col-md-2 formGroupMeio">
                <label for="serie">Série</label>
                <input type="text" class="form-control" id="serie" name="serie" placeholder="ex:1º"   @if(isset($filtros)) value="{{$filtros['serie']}} " @endif>
            </div>
            <div class="form-group col-md-3 formGroupMeio">
                <label for="turno">Turno</label>
                <input type="text" class="form-control" id="turno" name="turno" placeholder="ex: Manhã"   @if(isset($filtros)) value="{{$filtros['turno']}} " @endif>
            </div>
            <div class="form-group col-md-3 formGroupMeio">
                <label for="escola">Escola</label>
                <input type="text" class="form-control" id="escola" name="escola" placeholder="Nome da escola"   @if(isset($filtros)) value="{{$filtros['escola']}} " @endif>
            </div>
        </div>
        <div class="buttonRight">
            <button class="btn btn-primary">Pesquisar</button>
            <a href="{{route('turmas')}}" class="btn btn-secondary">Limpar</a>
        </div>
    </form>
    <!--Fim Filtro-->
    @if(session('statusFalha'))
        <div class="alert alert-danger">
            {{ session('statusFalha') }}
        </div>
    @elseif (session('statusSucesso'))
        <div class="alert alert-success">
            {{ session('statusSucesso') }}
        </div>
    @endif
    @if(!count($turmas) && isset($pesquisaRetornouVazio))
        <div class="alert alert-danger">
            Nenhuma turma encontrada com os parâmetros passados na pesquisa.
        </div>
    @else
    <div class="card-header textoBold">TURMAS</div>
        <div class="table-responsive">
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ano</th>
                    <th scope="col">Nivél</th>
                    <th scope="col">Série</th>
                    <th scope="col">turno</th>
                    <th scope="col">Escola</th>
                    <th scope="col">Atualizações</th>
                    <th scope="col">
                        <div class=".col-lg-12 buttonRight">
                            <a href="{{route('new.turma')}}" class="btn btn-primary btn-sm">Add Nova Turma</a>
                        </div>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($turmas as $t)
                    <tr class="table-sem-quebra">
                        <th scope="row">{{++$i}}</th>
                        <td>{{$t->ano}}</td>
                        <td>{{$t->nivel}}</td>
                        <td>{{$t->serie}}</td>
                        <td>{{$t->turno}}</td>
                        <td>@if(isset($filtros)) {{$t->escola}} @else {{$t->escola->escola}} @endif</td>
                        <td>{{ ucfirst(date("d/m/Y H:i", strtotime($t->updated_at)))}}</td>
                        <td>
                            <div class=".col-lg-12 buttonRight">
                            <a href="{{ route('edit.turma', ['turma_id'=>$t->id]) }}" class="btn btn-outline-primary btn-sm">Editar</a>
                            {{-- <button type="button" class="btn btn-outline-danger btn-sm"  data-bs-toggle="modal" data-bs-target="#exampleModal">Ecluir</button> --}}
                            <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#staticBackdrop{{$t->id}}">
                                Excluir
                              </button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if(isset($filtros))
                {{$turmas->appends($filtros)->links()}}
            @else
                {{$turmas->links()}}
            @endif
        </div>
    </div>
    @endif
</div>
@foreach($turmas as $t)
@include('/modals/deleteModalTurma')
@endforeach
<script src="/js/ctrl/escolas/pesquisa.js"></script>


 @endsection