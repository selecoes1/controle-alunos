<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'DashboardController@dashboard')->name('home');
//alunos
Route::get('/alunos', 'AlunosController@index')->name('alunos');
Route::get('/filtrar_alunos', 'AlunosController@filtrarAlunos')->name('filtrarAlunos');
Route::get('/novo/aluno', 'AlunosController@create')->name('new.aluno');
Route::post('/novo/aluno', 'AlunosController@store')->name('store.aluno');
Route::get('/editar/aluno', 'AlunosController@edit')->name('edit.aluno');
Route::post('/update/aluno', 'AlunosController@update')->name('update.aluno');
Route::get('/excluir/aluno', 'AlunosController@destroy')->name('delete.aluno');
//escolas
Route::get('/escolas', 'EscolasController@index')->name('escolas');
Route::get('/filtrar_escolas', 'EscolasController@filtrarEscolas')->name('filtrarEscolas');
Route::get('/nova/escola', 'EscolasController@create')->name('new.escola');
Route::post('/nova/escola', 'EscolasController@store')->name('store.escola');
Route::get('/show/escola', 'EscolasController@show')->name('show.escola');
Route::get('/editar/escola', 'EscolasController@edit')->name('edit.escola');
Route::post('/update/escola', 'EscolasController@update')->name('update.escola');
Route::get('/excluir/escola', 'EscolasController@destroy')->name('delete.escola');
//turmas
Route::get('/turmas', 'TurmasController@index')->name('turmas');
Route::get('/filtrar_turmas', 'TurmasController@filtrarTurmas')->name('filtrarTurmas');
Route::get('/nova/turma', 'TurmasController@create')->name('new.turma');
Route::post('/nova/turma', 'TurmasController@store')->name('store.turma');
Route::get('/editar/turma', 'TurmasController@edit')->name('edit.turma');
Route::post('/update/turma', 'TurmasController@update')->name('update.turma');
Route::get('/excluir/turma', 'TurmasController@destroy')->name('delete.turma');