# ChangeLog

## v0.0.7 - Feb. 24, 2022

new release.

**added:**
- update nas tabelas e exclusão em turmas e escolas sem tratamento no momento e alunos ainda só vinculados a uma turma.

## v0.0.1 - Feb. 19, 2022

Initial release.

**added:**
- Readme
- ChangeLog